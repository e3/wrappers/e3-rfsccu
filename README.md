# e3-rfsccu

EPICS StreamDevice e3 module for Temperature Compensating Unit - T6TCU02 from AFT microwave

Wrapper for the module rfsccu

<!-- This README.md should be updated as part of creation and should add complementary information about the wrapped module in question (usage, etc.). Once the repository is set up, empty/unused directories should also be purged. -->

## Requirements

<!-- Put requirements here, like:
- libusb
- ...
-->

## EPICS dependencies

<!-- Run `make dep` and put the results here, like:
```sh
$ make dep
require examplemodule,1.0.0
< configured ...
COMMON_DEP_VERSION = 1.0.0
> generated ...
common 1.0.0
```
-->

## Installation

```sh
$ make init patch build
$ make install
```

For further targets, type `make`.

## Usage

```sh
require rfsccu

iocshLoad("$(rfsccu_DIR)rfsccu.iocsh", "IPP=1.0.0.0:1, P=Sys-Sub:, R=Dis-Dev-Idx:")
```

where **IPP** is the macro for the IP(or HOSTNAME):PORT of the MOXA Box. **P** for the System Structure and **R** for the Device Structure.

The default scan of all the TCU parameters is 1 second. This value can be changed adding the macro **SCAN** in the iocshLoad command:

```sh
require rfsccu

iocshLoad("$(rfsccu_DIR)rfsccu.iocsh", "IPP=1.0.0.0:1, P=Sys-Sub:, R=Dis-Dev-Idx:, SCAN=2 second")
```
## Additional information

<!-- Put design info or links (where the real pages could be in e.g. `docs/design.md`, `docs/usage.md`) to design info here.
-->

## Contributing

Contributions through pull/merge requests only.
