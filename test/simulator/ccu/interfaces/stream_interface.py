from lewis.adapters.stream import StreamInterface, Cmd


class CCUStreamInterface(StreamInterface):
    commands = {
        Cmd(
            "read_info",
            pattern="M",
            return_mapping=lambda dev_info: f"M Tin={dev_info[0]}; Tout={dev_info[1]}; Tamb= {dev_info[2]}; dT= {dev_info[3]}; Current= {dev_info[4]}; RF= {dev_info[5]};",  # noqa: E501
        )
    }

    in_terminator = "\r"
    out_terminator = "\r"

    def handle_error(self, request, error):
        self.log.error(f"Communication error occurred at {request!r}: {error!r}")

    def read_info(self):
        return (
            self.device.water_inlet_temperature,
            self.device.water_outlet_temperature,
            self.device.ambient_temperature,
            self.device.potentiometer,
            self.device.coil_current,
            self.device.reflected_power,
        )
