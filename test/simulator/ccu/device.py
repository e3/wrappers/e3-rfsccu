from collections import OrderedDict

from lewis.devices import StateMachineDevice

from .states import DefaultState


class CCUSimulator(StateMachineDevice):
    def _initialize_data(self):
        self.water_inlet_temperature = 30.0
        self.water_outlet_temperature = 4.0
        self.ambient_temperature = 35.0
        self.potentiometer = 100
        self.coil_current = 10.0
        self.reflected_power = 25.0

    def _get_state_handlers(self):
        return {DefaultState.NAME: DefaultState()}

    def _get_initial_state(self):
        return DefaultState.NAME

    def _get_transition_handlers(self):
        return OrderedDict()
