require rfsccu

# Device
epicsEnvSet(P, "TestCCU:")
epicsEnvSet(R, "")

# MOXA
epicsEnvSet(MHOST, "localhost:9999")

iocshLoad("$(rfsccu_DIR)rfsccu.iocsh", "IPP=$(MHOST), P=$(P), R=$(R), SCAN=Passive")
