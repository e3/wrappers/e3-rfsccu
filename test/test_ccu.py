from pathlib import Path
import subprocess
from time import sleep

import pytest
from p4p.client.thread import Context
from run_iocsh import IOC


CTXT = Context("pva")
TEST_PATH = Path(__file__).absolute().parent


@pytest.fixture(scope="session")
def ioc():
    simulator_proc = subprocess.Popen(
        ["lewis", "-a", TEST_PATH, "-k", "simulator", "-p", "stream", "ccu"],
    )

    yield IOC((TEST_PATH / "cmds" / "test.cmd").as_posix())

    simulator_proc.terminate()


class TestIOCConnection:

    pv_wait_in_seconds = 10
    sleep_in_seconds = 1

    @pytest.mark.parametrize("num_runs", range(5))
    def test_connect(self, ioc: IOC, num_runs: int) -> None:
        with ioc:
            sleep(self.sleep_in_seconds)
            assert ioc.is_running()

    @pytest.mark.parametrize("num_runs", range(5))
    def test_disconnect(self, ioc: IOC, num_runs: int) -> None:
        with ioc:
            sleep(self.sleep_in_seconds)
        assert not ioc.is_running()


class TestReadWrite:

    pv_wait_in_seconds = 10
    sleep_in_seconds = 1

    @pytest.mark.parametrize(
        "pv_name, expected_val",
        [
            ("TestCCU:TempWaterInlet", 30.0),
            ("TestCCU:TempWaterOutlet", 4.0),
            ("TestCCU:TempAmbient", 35.0),
            ("TestCCU:DTPotentiometer", 100),
            ("TestCCU:Current", 10.0),
            ("TestCCU:RFPowerRefl", 25.0),
        ],
    )
    def test_readout(self, ioc: IOC, pv_name: str, expected_val: float) -> None:
        with ioc:
            sleep(self.sleep_in_seconds)

            # Force data processing
            CTXT.put("TestCCU:TempWaterInlet.PROC", 1, timeout=self.pv_wait_in_seconds)

            actual = CTXT.get(pv_name, timeout=self.pv_wait_in_seconds)
            assert actual == expected_val
